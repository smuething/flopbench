#ifndef ALLOCATOR_HH
#define ALLOCATOR_HH

#include <cstdlib>
#include <exception>

template<typename T, std::size_t alignment>
struct aligned_allocator
{

  static_assert(alignment >= sizeof(void*), "alignment too small");
  static_assert((alignment != 0) && not (alignment & (alignment - 1)), "alignment must be a power of 2");

  template<typename U>
  struct rebind
  {
    using other = aligned_allocator<U,alignment>;
  };

  using value_type = T;

  aligned_allocator() = default;

  template<typename U>
  aligned_allocator(const aligned_allocator<U,alignment>&) noexcept
  {}

  [[nodiscard]] T* allocate(std::size_t n)
  {
    if (n > std::size_t(-1) / sizeof(T))
      throw std::bad_alloc();
    void* p;
    if (posix_memalign(&p,alignment,n*sizeof(T)))
      throw std::bad_alloc();
    return static_cast<T*>(p);
  }

  void deallocate(T* p, std::size_t)
  {
    std::free(p);
  }

};

template<typename T, typename U, std::size_t alignment>
bool operator==(const aligned_allocator<T,alignment>&, const aligned_allocator<U,alignment>&)
{
  return true;
}

template<typename T, typename U, std::size_t alignment>
bool operator!=(const aligned_allocator<T,alignment>&, const aligned_allocator<U,alignment>&)
{
  return false;
}

#endif // ALLOCATOR_HH
