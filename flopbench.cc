#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <random>
#include <algorithm>
#include <iterator>
#include <functional>

#include "vectorclass.h"
#include "allocator.hh"
#include "clara.hh"

#if HWLOC
#include <hwloc.h>
#endif

#if MAX_VECTOR_SIZE == 256
using Vector = Vec4d;
constexpr std::size_t vector_factor = 4;
#elif MAX_VECTOR_SIZE == 512
using Vector = Vec8d;
constexpr std::size_t vector_factor = 8;
#else
#error invalid SIMD width
#endif

#ifndef LSIZE
#define LSIZE 4
#endif

#ifndef CSIZE
#define CSIZE 4
#endif

#ifndef RSIZE
#define RSIZE 16
#endif



template<std::size_t i>
using Dim = std::integral_constant<std::size_t,i>;


namespace vecbased {


  template<std::size_t m, std::size_t k>
  struct row_holder
    : public row_holder<m,k+1>
  {

    using idx_t = std::size_t;
    using Scalar = double;

    static const idx_t row_stride = 1;

    row_holder(const Vector* __restrict__ A, idx_t i)
      : row_holder<m,k+1>(A,i)
    {
      _a = A[k + m*i];
    }


    template<typename RHS>
    Vector matmul(const RHS* __restrict__ B, idx_t l) const
    {
      Vector r(0.0);
      // start calculation by setting the pointer to the correct position
      return _matmul(B + l*m,r);
    }

    template<typename RHS>
    Vector _matmul(const RHS* __restrict__ B, Vector r) const
    {
      Vector b;
      // get B_{j,l}
      b = B[k * row_stride];
      // go to A_{i,j+1}, B_{j+1,l}
      return row_holder<m,k+1>::_matmul(B,mul_add(_a,b,r));
    }

    Vector _a;

  };

  /** \brief end of recursion

      \tparam k    column size of left matrix / row size of right matrix
  */

  template<std::size_t m>
  struct row_holder<m,m>
  {

    using idx_t = std::size_t;

    row_holder(const Vector* __restrict__ A, idx_t i)
    {}

    //! last function call in matmul, returns scalar product between i-th row of A
    //! and l-th column of B
    template<typename RHS>
    Vector _matmul(const RHS* __restrict__ B, Vector r) const
    {
      return r;
    }

  };

  template<std::size_t m>
  row_holder<m,0> load_row(const Vector* __restrict__ A, std::size_t i, Dim<m>)
  {
    return {A,i};
  }




} // namespace vecbased




struct worker
{


  template<typename S>
  worker(std::size_t iterations, S& seed)
    : _iterations(iterations)
    , _in(CSIZE*RSIZE)
    , _out(CSIZE*RSIZE,{0.0})
    , _mat(LSIZE*CSIZE)
  {
    std::mt19937_64 rng(seed);
    std::uniform_real_distribution<double> dist(-1.0,1.0);
    auto gen = [&]() { return dist(rng); };
    std::generate(begin(_in),end(_in),gen);
    std::generate(begin(_mat),end(_mat),gen);
  }


#if HWLOC
  template<typename S>
  worker(
    std::size_t iterations, S& seed,
    bool no_pinning, bool pin_to_core,
    const hwloc_topology_t& topology,
    hwloc_obj_type_t type,
    std::size_t idx
    )
    : worker(iterations,seed)
  {
    if (no_pinning)
      return;
    hwloc_obj_t core = hwloc_get_obj_by_type(topology,type,idx);
    hwloc_cpuset_t set = hwloc_bitmap_dup(core->cpuset);
    if (not pin_to_core)
      hwloc_bitmap_singlify(set);
    _setup = [=]() {
      hwloc_set_cpubind(topology,set,HWLOC_CPUBIND_THREAD);
      hwloc_bitmap_free(set);
    };
  }
#endif


  void operator()()
  {

    if (_setup)
      _setup();

    Vector* __restrict__ in = _in.data();
    Vector* __restrict__ out = _out.data();
    Vector* __restrict__ mat = _mat.data();

    for (std::size_t i = 0 ; i < _iterations ; ++i)
      {
        for (std::size_t j = 0, o = 0 ; j < LSIZE ; ++j)
          {
            auto row = vecbased::load_row(mat,j,Dim<CSIZE>());
            for (std::size_t k = 0 ; k < RSIZE ; ++k)
              {
                Vector r = row.matmul(in,k);
                out[o++] = r;
              }
          }
        for (std::size_t j = 0, o = 0 ; j < LSIZE ; ++j)
          {
            auto row = vecbased::load_row(mat,j,Dim<CSIZE>());
            for (std::size_t k = 0 ; k < RSIZE ; ++k)
              {
                Vector r = row.matmul(out,k);
                in[o++] = r;
              }
          }
      }
  }

  constexpr static std::size_t ops_per_iteration()
  {
    return 2*2*vector_factor*LSIZE*CSIZE*RSIZE;
  }

  const std::size_t _iterations;
  std::function<void()> _setup;
  std::vector<Vector,aligned_allocator<Vector,64>> _in;
  std::vector<Vector,aligned_allocator<Vector,64>> _out;
  std::vector<Vector,aligned_allocator<Vector,64>> _mat;

};


constexpr std::size_t ul_pow(std::size_t base, std::size_t exp)
{
  std::size_t r = 1;
  for (std::size_t i = 0 ; i < exp ; ++i)
    r *= base;
  return r;
}


int main(int argc, char** argv)
{
  bool help = false;
  std::size_t iterations_exp = 5;
#if HWLOC
  bool task_per_pu = false;
  bool no_pinning = false;
  bool pin_to_core = false;
#else
  std::size_t pu_factor = 1;
#endif

  std::size_t concurrency = 0;

  auto cli
    = clara::Arg(iterations_exp, "iterations_exp")
        ("Benchmark will do 10^iterations_exp iterations")
        .required()
    | clara::Opt(concurrency, "tasks")
        ["-t"]["--tasks"]
        ("Number of concurrent tasks to start")
#if HWLOC
    | clara::Opt(task_per_pu)
        ["-p"]["--task-per-pu"]
        ("Assume one task per PU instead of per core")
    | clara::Opt(no_pinning)
        ["-n"]["--no-pinning"]
        ("Don't pin tasks")
    | clara::Opt(pin_to_core)
        ["-c"]["--pin-to-core"]
      ("Always pin to full cores (not compatible with -p)")
#else
    | clara::Opt(pu_factor,"pu_factor")
        ["-f"]["--pu-factor"]
        ("Number of PUs per core")
#endif
    | clara::Help(help);

  auto args = cli.parse(clara::Args(argc,argv));
  if (!args)
    {
      std::cerr << "Error in command line: " << args.errorMessage() << std::endl;
      std::cerr << cli << std::endl;
      std::exit(1);
    }

  if (help)
    {
      std::cerr << cli << std::endl;
      std::exit(0);
    }

  auto iterations = ul_pow(10,iterations_exp);

#if HWLOC

  std::cout << "Initializing hwloc topology... " << std::flush;
  hwloc_topology_t topology;
  hwloc_topology_init(&topology);
  hwloc_topology_load(topology);
  std::cout << "done" << std::endl;

  if (no_pinning)
    std::cout << "Task pinning disabled" << std::endl;

  if (pin_to_core)
    std::cout << "Pinning tasks to cores instead of PUs" << std::endl;

  std::cout << "Assuming one task per " << (task_per_pu ? "PU" : "core") << std::endl;

  auto obj_type = task_per_pu ? HWLOC_OBJ_PU : HWLOC_OBJ_CORE;

  if (concurrency == 0)
    concurrency = hwloc_get_nbobjs_by_type(topology,obj_type);

#else

  std::cout << "hwloc not found, task pinning disabled" << std::endl;
  std::cout << "PU factor: " << pu_factor << std::endl;

  if (concurrency == 0)
    concurrency = std::thread::hardware_concurrency() / pu_factor;

#endif

  std::cout << "Number of concurrent tasks: " << concurrency << std::endl;
  std::cout << "Doing 10^" << iterations_exp << " iterations" << std::endl;
  std::cout << "SIMD width: " << MAX_VECTOR_SIZE << std::endl;
  std::cout << "Measuring (" << LSIZE << " x " << CSIZE << ") * (" << CSIZE << " x " << RSIZE << ")" << std::endl;

  std::vector<std::thread> threads(concurrency);

  std::seed_seq seed = {1,2,3,4,5,6,7,8,9,10};

#if HWLOC
  std::size_t idx = 0;
  for (auto& thread : threads)
    thread = std::thread(worker(iterations,seed,no_pinning,pin_to_core,topology,obj_type,idx++));
#else
  for (auto& thread : threads)
    thread = std::thread(worker(iterations,seed));
#endif

  auto start = std::chrono::high_resolution_clock::now();
  for (auto& thread : threads)
    thread.join();
  auto end = std::chrono::high_resolution_clock::now();

  auto ops = concurrency * worker::ops_per_iteration() * iterations;
  std::cout << "Total operations: " << double(ops) << std::endl;
  auto elapsed = std::chrono::duration<double>((end-start)).count();
  std::cout << "elapsed: " << elapsed << std::endl;
  std::cout << "GFLOPs  total: " << ops / elapsed / 1.0e9 << std::endl;
  std::cout << "GFLOPs / task: " << ops / elapsed / 1.0e9 / concurrency << std::endl;
  return 0;
}
